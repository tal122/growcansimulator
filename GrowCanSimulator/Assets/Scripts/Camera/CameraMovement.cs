﻿// Just add this script to your camera. It doesn't need any configuration.

using System.Collections;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    Vector2?[] oldTouchPositions = { null, null };
    Vector2 oldTouchVector;
    float oldTouchDistance;
    Camera mainCamera;
    Touch initTouch;            //new
    float rotX = 0;             //new   
    float rotY = 0;             //new
    Vector3 originalRot;        //new
    float rotSpeed = 0.025f;     //new
    float[] lastDistances = new float[4];
    float speed = 25;
    //[SerializeField] private GameObject roomBounds;
    Vector3 cameraBoundsPosition;
    Bounds cameraBounds;
    [SerializeField] private cameraPositionState eCameraPositionState;

    [SerializeField] private GameObject currentMainCameraGameObject;
    private Camera currentMainCamera;
    private bool isMoveable;
    private float growingRoomCameraLastDirection = 0;
    private Coroutine cameraPositionCoroutine = null;
    private Coroutine cameraRotationCoroutine = null;
    private Coroutine cameraFieldOfViewCoroutine = null;

    [Header("GrowingRoom")]
    [SerializeField] private GameObject cameraAxisOfPlant;
    [SerializeField] private GameObject[] growingRoomColliders;
    [SerializeField] private Transform growingRoomHorizontalView;
    [SerializeField] private Transform growingRoomClosetHorizontalView;
    [SerializeField] private Transform growingRoomWaterHorizontalView;
    [SerializeField] private Transform growingRoomCalendarHorizontalView;
    [SerializeField] private Canvas BackButton;
    [SerializeField] private Vector2 cameraRotationSpeed;
    [SerializeField] private float cameraZoomSpeed;

    private Vector3 newLocalRotation;
    private float initialCurrentCameraFieldOfView;

    private void Awake()
    {
        currentMainCamera = currentMainCameraGameObject.GetComponent<Camera>();
        initialCurrentCameraFieldOfView = currentMainCamera.fieldOfView;
        currentMainCamera.transform.position = growingRoomHorizontalView.position;
        currentMainCamera.transform.rotation = growingRoomHorizontalView.rotation;
        mainCamera = GetComponent<Camera>();
        eCameraPositionState = cameraPositionState.GrowingRoom;
    }

    private void Start()
    {
        originalRot = currentMainCamera.transform.eulerAngles; //new
        rotX = originalRot.x;                           //new
        rotY = originalRot.y;                           //new
    }

    private void MoveCameraToTransform(Transform finishTransform, float time)
    {
        cameraPositionCoroutine = StartCoroutine(LerpToPosition(finishTransform, time));
        cameraRotationCoroutine = StartCoroutine(LerpToRotation(finishTransform, time));
        cameraFieldOfViewCoroutine = StartCoroutine(LerpToFieldOfView(time));
    }

    private void MoveCameraToTransform(Transform finishTransform, Vector3 finishPoint, float time)
    {
        cameraPositionCoroutine = StartCoroutine(LerpToPosition(finishTransform, time));
        cameraRotationCoroutine = StartCoroutine(LerpToRotation(finishPoint, time));
        cameraFieldOfViewCoroutine = StartCoroutine(LerpToFieldOfView(time));
    }

    private IEnumerator LerpToRotation(Vector3 finishPoint, float LerpTime)
    {
        if (currentMainCamera == null)
            yield return null;

        float StartTime = Time.time;
        float EndTime = StartTime + LerpTime;

        while (Time.time < EndTime)
        {
            float timeProgressed = (Time.time - StartTime) / LerpTime;
            Vector3 targetDirection = finishPoint - currentMainCamera.transform.position;
            Vector3 newDirection = Vector3.RotateTowards(currentMainCamera.transform.forward, targetDirection, timeProgressed * Time.deltaTime, 0.0F);
            currentMainCamera.transform.rotation = Quaternion.LookRotation(newDirection);
            yield return null;
        }

        cameraRotationCoroutine = null;
    }

    private IEnumerator LerpToRotation(Transform finishTransform, float LerpTime)
    {
        if (currentMainCamera == null)
            yield return null;

        float StartTime = Time.time;
        float EndTime = StartTime + LerpTime;
        Quaternion startQuaternion = currentMainCamera.transform.rotation;

        while (Time.time < EndTime)
        {
            float timeProgressed = (Time.time - StartTime) / LerpTime;
            currentMainCamera.transform.rotation = Quaternion.Lerp(startQuaternion, finishTransform.rotation, timeProgressed);
            yield return null;
        }

        cameraRotationCoroutine = null;
    }

    private IEnumerator LerpToFieldOfView(float LerpTime)
    {
        if (currentMainCamera == null)
            yield return null;

        float StartTime = Time.time;
        float EndTime = StartTime + LerpTime;
        float startCameraFieldOfView = currentMainCamera.fieldOfView;

        while (Time.time < EndTime)
        {
            float timeProgressed = (Time.time - StartTime) / LerpTime;
            currentMainCamera.fieldOfView = Mathf.Lerp(startCameraFieldOfView, initialCurrentCameraFieldOfView, timeProgressed);
            yield return null;
        }

        cameraFieldOfViewCoroutine = null;
    }

    private IEnumerator LerpToPosition(Transform finishTransform, float LerpTime)
    {
        if (currentMainCamera == null)
            yield return null;

        float StartTime = Time.time;
        float EndTime = StartTime + LerpTime;
        Vector3 startPosition = currentMainCamera.transform.position;
        
        while (Time.time < EndTime)
        {
            float timeProgressed = (Time.time - StartTime) / LerpTime;
            currentMainCamera.transform.position = Vector3.Lerp(startPosition, finishTransform.position, timeProgressed);
            yield return null;
        }

        cameraPositionCoroutine = null;
    }

    //private IEnumerator MoveToTransform(Transform finishTransform, float LerpTime)
    //{
    //    if (currentMainCamera == null)
    //        yield return null;

    //    float StartTime = Time.time;
    //    float EndTime = StartTime + LerpTime;
    //    Vector3 startPosition = currentMainCamera.transform.position;
    //    Quaternion startQuaternion = currentMainCamera.transform.rotation;
    //    float startCameraFieldOfView = currentMainCamera.fieldOfView;

    //    while (Time.time < EndTime)
    //    {
    //        float timeProgressed = (Time.time - StartTime) / LerpTime;
    //        currentMainCamera.transform.position = Vector3.Lerp(startPosition, finishTransform.position, timeProgressed);
    //        currentMainCamera.transform.rotation = Quaternion.Lerp(startQuaternion, finishTransform.rotation, timeProgressed);
    //        currentMainCamera.fieldOfView = Mathf.Lerp(startCameraFieldOfView, initialCurrentCameraFieldOfView, timeProgressed);
    //        yield return null;
    //    }

    //    cameraCoroutine = null;
    //}

    private void MoveCameraWorldAxis(Vector3 axis)
    {
        if (Input.touchCount == 2)
        {
            float distanceMagnitude = Input.GetTouch(0).deltaPosition.y + Input.GetTouch(1).deltaPosition.y;

            cameraAxisOfPlant.transform.localPosition = new Vector3(
                cameraAxisOfPlant.transform.localPosition.x, 
                Mathf.Lerp(
                    cameraAxisOfPlant.transform.localPosition.y,
                    cameraAxisOfPlant.transform.localPosition.y - distanceMagnitude, 
                    Time.deltaTime * 0.12f),
                cameraAxisOfPlant.transform.localPosition.z);

            currentMainCamera.transform.localPosition = new Vector3(
                currentMainCamera.transform.localPosition.x,
                Mathf.Lerp(
                    currentMainCamera.transform.localPosition.y,
                    currentMainCamera.transform.localPosition.y - distanceMagnitude,
                    Time.deltaTime * 0.12f),
                currentMainCamera.transform.localPosition.z);
        }
    }

    private void RotateCameraLocal()
    {
        if (Input.touchCount == 1)
        {
            if (Input.touches[0].phase == TouchPhase.Moved) //rotate with 1 finger
            {
                if (Input.touches[0].deltaPosition.x > 0 && currentMainCamera.transform.localEulerAngles.y < 110 || //slide left
                    Input.touches[0].deltaPosition.x < 0 && currentMainCamera.transform.localEulerAngles.y > 15)    //slide right
                {
                    currentMainCamera.transform.RotateAround(
                        new Vector3(cameraAxisOfPlant.transform.position.x, currentMainCamera.transform.position.y, cameraAxisOfPlant.transform.position.z),
                        Vector3.up * Input.touches[0].deltaPosition.x,
                        Time.deltaTime * 30);
                }

                if (Input.touches[0].deltaPosition.y < 0 && (currentMainCamera.transform.localEulerAngles.x > 200 || currentMainCamera.transform.localEulerAngles.x < 55) ||    //slide up
                    Input.touches[0].deltaPosition.y > 0 && (currentMainCamera.transform.localEulerAngles.x > 350 || currentMainCamera.transform.localEulerAngles.x < 200))     //slide down
                {
                    currentMainCamera.transform.RotateAround(
                      cameraAxisOfPlant.transform.position,
                      -currentMainCamera.transform.right * Input.touches[0].deltaPosition.y,
                      Time.deltaTime * 10);
                }
            }            
        }
    }

    private void ZoomCameraLocalForward()
    {
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPosition = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPosition = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMagnitude = (touchZeroPrevPosition - touchOnePrevPosition).magnitude;
            float touchDeltaMagnitude = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMagnitude - touchDeltaMagnitude;

            currentMainCamera.fieldOfView += deltaMagnitudeDiff * cameraZoomSpeed;
            currentMainCamera.fieldOfView = Mathf.Clamp(currentMainCamera.fieldOfView, 1, 70);
        }
    }

    public void ChangeCameraPositionStateToRoom()
    {
        ChangeCameraPositionState(cameraPositionState.GrowingRoom);
    }

    private IEnumerator EndOfCameraMovementActions(bool isEnableBackButton, bool isEnableGrowingRoomColliders)
    {
        while (cameraPositionCoroutine != null ||
            cameraRotationCoroutine != null ||
            cameraFieldOfViewCoroutine != null)
            yield return null;

        BackButton.gameObject.SetActive(isEnableBackButton);
        EnableAllGrowingRoomColliders(isEnableGrowingRoomColliders);
    }

    public void ChangeCameraPositionState(cameraPositionState cameraPositionState)
    {
        if (eCameraPositionState != cameraPositionState && 
            cameraPositionCoroutine == null && 
            cameraRotationCoroutine == null && 
            cameraFieldOfViewCoroutine == null)
        {
            isMoveable = false;
            eCameraPositionState = cameraPositionState;
            switch (cameraPositionState)
            {
                case cameraPositionState.Menu:
                    break;
                case cameraPositionState.GrowingRoom:
                    {
                        MoveCameraToTransform(growingRoomHorizontalView, 1);
                        StartCoroutine(EndOfCameraMovementActions(false, true));
                        break;
                    }
                case cameraPositionState.GrowingCloset:
                    {
                        MoveCameraToTransform(growingRoomClosetHorizontalView, cameraAxisOfPlant.transform.position, 1);
                        StartCoroutine(EndOfCameraMovementActions(true, false));
                        break;
                    }
                case cameraPositionState.GrowingDesk:
                    {
                        MoveCameraToTransform(growingRoomWaterHorizontalView, 1);
                        StartCoroutine(EndOfCameraMovementActions(true, false));
                        break;
                    }
                case cameraPositionState.Calendar:
                    {
                        MoveCameraToTransform(growingRoomCalendarHorizontalView, 1);
                        StartCoroutine(EndOfCameraMovementActions(true, false));
                        break;
                    }
                case cameraPositionState.Store:
                    break;
                default:
                    break;
            }
        }
    }

    private void EnableAllGrowingRoomColliders(bool isEnable)
    {
        for (int i = 0; i < growingRoomColliders.Length; i++)
        {
            growingRoomColliders[i].GetComponent<Collider>().enabled = isEnable;//.gameObject.SetActive(isEnable);
        }
    }
    
    void LateUpdate()
    {
        if (cameraPositionCoroutine == null &&
            cameraRotationCoroutine == null &&
            cameraFieldOfViewCoroutine == null)
        {
            switch (eCameraPositionState)
            {
                case cameraPositionState.Menu:
                    break;
                case cameraPositionState.GrowingRoom:
                    break;
                case cameraPositionState.GrowingCloset:
                    {
                        RotateCameraLocal();
                        if (Input.touchCount == 2)
                        {
                            if (Vector3.Angle(Input.GetTouch(0).deltaPosition, Input.GetTouch(1).deltaPosition) < 30)   //(Input.GetTouch(0).deltaPosition  Input.GetTouch(1).deltaPosition)
                            {
                                MoveCameraWorldAxis(Vector3.up);
                            }
                            else
                            {
                                ZoomCameraLocalForward();
                            }
                        }
                        break;
                    }
                case cameraPositionState.GrowingDesk:
                    break;
                case cameraPositionState.Store:
                    break;
                default:
                    break;
            }
        }
	}
}

public enum cameraPositionState
{
    Menu,
    GrowingRoom,
    GrowingCloset,
    GrowingDesk,
    Calendar,
    Store
}