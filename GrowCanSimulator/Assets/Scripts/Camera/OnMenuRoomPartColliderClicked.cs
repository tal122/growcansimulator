﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMenuRoomPartColliderClicked : MonoBehaviour
{
    [SerializeField] private CameraMovement cameraMovement;
    [SerializeField] private cameraPositionState nextCameraPositionState;

    private void OnMouseDown()
    {
        if (cameraMovement != null)
        {
            cameraMovement.ChangeCameraPositionState(nextCameraPositionState);
        }
    }
}