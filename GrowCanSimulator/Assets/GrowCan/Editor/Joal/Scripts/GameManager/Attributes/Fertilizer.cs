﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum TypeOfFertilizer
{
    Coco,
    PowerZim,
    RootJuice,
    Booster,
    Booster2,
    PK
}

[Serializable]
public class Fertilizer : MonoBehaviour
{
    [Help("0 = Coco,\n1 = PowerZim,\n2 = RootJuice,\n3 = Booster,\n4 = Booster2,\n5 = PK", MessageType.None)]
    [SerializeField] private int nothing;
    [SerializeField] private List<int> FertilizerTypeAmounts;

    public void AddFertilizerAmount(TypeOfFertilizer typeOfFertilizer, int amountToAdd)
    {
        int index = (int) typeOfFertilizer;
        if (FertilizerTypeAmounts != null)
        {
            FertilizerTypeAmounts[index] += amountToAdd;
        }
    }
    public int GetFertilizerAmount(TypeOfFertilizer typeOfFertilizer)
    {
        return FertilizerTypeAmounts[(int) typeOfFertilizer];
    }
}