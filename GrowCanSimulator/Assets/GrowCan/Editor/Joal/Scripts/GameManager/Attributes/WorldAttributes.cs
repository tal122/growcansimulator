﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldAttributes : MonoBehaviour
{
    private int worldTemp;
    private int worldHumidity;

    public void SetWorldTemp(int _worldTemp)
    {
        worldTemp = _worldTemp;
    }

    public int GetWorldTemp()
    {
        return worldTemp;
    }

    public void SetWorldHumidity(int _worldHumidity)
    {
        worldHumidity = _worldHumidity;
    }

    public int GetWorldHumidity()
    {
        return worldHumidity;
    }
}