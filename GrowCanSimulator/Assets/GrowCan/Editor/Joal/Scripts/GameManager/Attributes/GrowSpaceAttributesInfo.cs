﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GrowSpaceAttributesInfo : MonoBehaviour {

    
    [Header("Current Attributes")]
    #region humidity
    [SerializeField] private int humidity;
    private int humidityConstantBonus = 0;
    private float humidityTempBonus = 0;
    private int humidityBonusRemoveNumberOfSteps = 1;
    #endregion

    #region temperature
    [SerializeField] private int temperature;
    private int temperatureConstantBonus = 0;
    private float temperatureTempBonus = 0;
    private int temperatureBonusRemoveNumberOfSteps = 1;
    #endregion

    [SerializeField] private int waterInGround;
    [SerializeField] private int airChanged;

    [Header("Water Supply pH")]
    [Range(0f,14f)] [SerializeField] private float waterPHLevel;

    [Header("Fan Ideal Spots")]
    [SerializeField] private List<Transform> idealFanSpotPositionsList;


    [Header("Gear")]
    [SerializeField] private ClosetLight closetLight;
    [SerializeField] private AirVenta airVenta;
    [SerializeField] private Steam steam;
    [SerializeField] private CoolTube coolTube;
    [SerializeField] private Fan fan;
    [SerializeField] private Shpritzer shpritzer;
    [SerializeField] private Fertilizer fertilizerInWater;

    private float totalElectricityConsumed = 0;
    private float waterPHLevelMin = 0f;
    private float waterPHLevelMax = 14f;

    void Start()
    {
        CheckForCoolTube();
    }
        
    #region Humidity
    public void AddHumidity(int add)
    {
        humidity += add;
    }

    public void AddTempHumidityBonus(int addAmount, float timeInHoursForEffect)
    {
        humidityTempBonus += addAmount;
        StartCoroutine(RemoveCertainHumidityAmountOverCertainTime(addAmount, timeInHoursForEffect, humidityBonusRemoveNumberOfSteps));
    }

    public void AddConstantHumidityBonus(int addAmount)
    {
        humidityConstantBonus += addAmount;
    }

    IEnumerator RemoveCertainHumidityAmountOverCertainTime(int amountToRemove,float timeInHours, int stepsNumber)
    {
        float amountToRemovePerStep = amountToRemove / stepsNumber;
        float timeForStep = timeInHours / stepsNumber;

        for (int i = 0; i < stepsNumber; i++)
        {
            yield return new WaitForSeconds(timeForStep * 3600);
            humidityTempBonus -= amountToRemovePerStep;
        }
    }

    public void SetHumidity(int _humidity)
    {
        humidity = _humidity;
    }

    public int GetHumidity()
    {
        return (int)(humidity + humidityConstantBonus + humidityTempBonus);
    }
    #endregion

    #region Temperature
    public void AddTemp(int add)
    {
        temperature += add;
    }

    public void SetTemp(int _temperature)
    {
        temperature = _temperature;
    }

    public int GetTemp()
    {
        return (int)(temperature + temperatureConstantBonus + temperatureTempBonus);
    }

    public void AddTempTemperatureBonus(int addAmount, float timeInHoursForEffect)
    {
        temperatureTempBonus += addAmount;
        StartCoroutine(RemoveCertainHumidityAmountOverCertainTime(addAmount, timeInHoursForEffect, temperatureBonusRemoveNumberOfSteps));
    }

    public void AddConstantTemperatureBonus(int addAmount)
    {
        temperatureConstantBonus += addAmount;
    }

    IEnumerator RemoveCertainTemperatureAmountOverCertainTime(int amountToRemove, float timeInHours, int stepsNumber)
    {
        float amountToRemovePerStep = amountToRemove / stepsNumber;
        float timeForStep = timeInHours / stepsNumber;

        for (int i = 0; i < stepsNumber; i++)
        {
            yield return new WaitForSeconds(timeForStep * 3600);
            temperatureTempBonus -= amountToRemovePerStep;
        }

    }
    #endregion

    #region WaterInGround
    public void AddWaterInGround(int add)
    {
        waterInGround += add;
    }

    public void SetWaterInGround(int _waterInGround)
    {
        waterInGround = _waterInGround;
    }

    public int GetWaterInGround()
    {
        return waterInGround;
    }
    #endregion

    #region WaterPH
    public void AddWaterPH(float phAmountAdd)
    {
        waterPHLevel += phAmountAdd;
        waterPHLevel = Mathf.Clamp(waterPHLevel, waterPHLevelMin, waterPHLevelMax);
    }

    public void SetWaterPH(float phAmount)
    {
        waterPHLevel = phAmount;
        waterPHLevel = Mathf.Clamp(waterPHLevel, waterPHLevelMin, waterPHLevelMax);
    }

    public float GetWaterPH()
    {
        return waterPHLevel;
    }
    #endregion

    #region Electricity
    public void ConsumeElectricity(float amountToConsume)
    {
        totalElectricityConsumed += amountToConsume;
    }
    #endregion

    public int GetAir()
    {
        return airChanged;
    }

    public Fertilizer GetFertilizer()
    {
        return fertilizerInWater;
    }

    public List<Transform> GetIdealFanPositionsList()
    {
        return idealFanSpotPositionsList;
    }

    private void CheckForCoolTube()
    {
        coolTube = GetComponentInChildren<CoolTube>();
        if (coolTube != null)
        {
            AddConstantTemperatureBonus(coolTube.GetTemperatureEffect());
        }
    }
}