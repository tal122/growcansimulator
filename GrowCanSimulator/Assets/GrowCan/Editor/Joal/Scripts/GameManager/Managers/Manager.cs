﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfWeek
{
    sprouting,
    growing,
    bloom,
    harvesting
}

public class Manager : MonoBehaviour
{
    [SerializeField] private List<WeekScoreManager> WeeksScoreManagers;
    [SerializeField] private GrowSpaceAttributesInfo growSpaceAttributesInfo;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            WeeksScoreManagers[0].CheckAndPrintScoreAtTheMoment(growSpaceAttributesInfo);
        }
    }
}