﻿using System;
using UnityEngine;

[Serializable]
public class WeekScoreManager
{
    [Header("Main Settings")]
    [SerializeField] private new string name;
    [SerializeField] private TypeOfWeek typeOfWeek;
    [SerializeField] private WeekAttributes thisWeekAttributes;

    public void CheckAndPrintScoreAtTheMoment(GrowSpaceAttributesInfo info)
    {
        Debug.Log(thisWeekAttributes.GetTotalWeekScore(info));
    }
}