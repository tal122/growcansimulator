﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FertilizerWeekScoreGroup : ScorableScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private TypeOfGradeGroup typeOfGradeGroup;
    [SerializeField] private List<TypeOfFertilizer> typesOfFertilizersToCheck;
    [SerializeField] private List<float> worstGroupValue;
    [SerializeField] private List<float> optimalGroupValue;
    [SerializeField] private List<float> minGradeValue;
    [SerializeField] private List<float> maxGradeValue;

    public bool IsInThisGroup(float value)
    {
        return true;
    }

    public bool IsInThisGroup(int value)
    {
        return IsInThisGroup((float)value);
    }

    public override float GetScore(GrowSpaceAttributesInfo info)
    {
        Fertilizer fertilizer = info.GetFertilizer();
        List<float> gradesList = new List<float>();
        for (int i = 0; i < worstGroupValue.Count-1; i++)
        {
            for (int j = 0; j < typesOfFertilizersToCheck.Count-1; j++)
            {
                if (i == (int) typesOfFertilizersToCheck[j])
                {
                    float valueInverseLerp = Mathf.InverseLerp(worstGroupValue[i], optimalGroupValue[i], fertilizer.GetFertilizerAmount((TypeOfFertilizer)i));
                    gradesList.Add(Mathf.Lerp(minGradeValue[i], maxGradeValue[i], valueInverseLerp));
                }
            }
        }

        float average = 0;
        foreach (var grade in gradesList)
        {
            average += grade;
        }

        average /= gradesList.Count;
        Debug.Log("The Score For " + name + " Is --> " + average);
        return (average * AttributesWeeklyScorePercent) / 100f;
    }
}