﻿using UnityEngine;
using UnityEditor;

public class ScriptableObjectsCreation
{
    [MenuItem("Assets/Create/new CreateSingleScoreGroup")]
    public static void CreateSingleScoreGroup()
    {
        ScriptableObjectUtility.CreateAsset<SingleScoreGroup>();
    }

    [MenuItem("Assets/Create/new FertilizerScoreGroup")]
    public static void CreateFertilizerAsset()
    {
        ScriptableObjectUtility.CreateAsset<FertilizerWeekScoreGroup>();
    }
    
    [MenuItem("Assets/Create/new SingleAttribute")]
    public static void CreateSingleAttribute()
    {
        ScriptableObjectUtility.CreateAsset<SingleAttribute>();
    }

    [MenuItem("Assets/Create/WeekAttributes")]
    public static void CreateWeekAttributes()
    {
        ScriptableObjectUtility.CreateAsset<WeekAttributes>();
    }

    [MenuItem("Assets/Create/Gear/Fan/new FanEffectGroup")]
    public static void CreateFanEffectGroup()
    {
        ScriptableObjectUtility.CreateAsset<FanEffectGroup>();
    }

    [MenuItem("Assets/Create/Gear/new VentaType")]
    public static void CreateVentaType()
    {
        ScriptableObjectUtility.CreateAsset<AirVentaTypeScriptableObject>();
    }

    [MenuItem("Assets/Create/Gear/new Steam Level")]
    public static void CreateSteamLevel()
    {
        ScriptableObjectUtility.CreateAsset<SteamLevelScriptableObject>();
    }
}