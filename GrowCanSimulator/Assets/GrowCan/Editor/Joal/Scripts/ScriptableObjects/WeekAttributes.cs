﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class WeekAttributes : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private List<ScorableScriptableObject> thisWeekAttributes;
    
    public float GetTotalWeekScore(GrowSpaceAttributesInfo info)
    {
        float totalGrowScore = 0;
        foreach (var singleAttribute in thisWeekAttributes)
        {
            totalGrowScore += GetScoreFromAttributeBasedOnType(singleAttribute, info);
        }

        return totalGrowScore;
    }

    private float GetScoreFromAttributeBasedOnType(ScorableScriptableObject singleAttribute,GrowSpaceAttributesInfo info)
    {
        return singleAttribute.GetScore(info);
    }
}