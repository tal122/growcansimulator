﻿using System;
using UnityEngine;

[Serializable]
public class FanEffectGroup : ScriptableObject
{
    private new string name;
    [SerializeField] private int humidityEffect;
    [SerializeField] private int temperatureEffect;
    [SerializeField] private float plantStrengthEffect;
    [SerializeField] private float minDistance;
    [SerializeField] private float maxDistance;

    public bool IsInThisGroup(float distance)
    {
        if (distance <= maxDistance && distance >= minDistance)
        {
            return true;
        }

        return false;
    }

    public int GetHumidityEffect()
    {
        return humidityEffect;
    }

    public int GetTemperatureEffect()
    {
        return temperatureEffect;
    }

    public float GetPlantStrengthEffect()
    {
        return plantStrengthEffect;
    }
}