﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorableScriptableObject : ScriptableObject
{
    [SerializeField] protected int AttributesWeeklyScorePercent;
    [SerializeField] protected TypeOfAttribute attributeType;

    public virtual float GetScore(GrowSpaceAttributesInfo info)
    {
        Debug.LogError("GetScore() isn't implemented --> use override");
        return 0f;
    }

    public TypeOfAttribute GetTypeOfAttribute()
    {
        return attributeType;
    }
}