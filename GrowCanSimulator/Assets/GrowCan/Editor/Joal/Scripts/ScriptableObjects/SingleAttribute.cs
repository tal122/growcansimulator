﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum TypeOfAttribute
{
    WaterInGround,
    Temperature,
    PH,
    Humidity,
    Air,
    Fertilizer
}

[Serializable]
public class SingleAttribute : ScorableScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private List<SingleScoreGroup> scoreGroups;
    public override float GetScore(GrowSpaceAttributesInfo info)
    {
        float score = 0f;
        switch (attributeType)
        {
            case TypeOfAttribute.WaterInGround:
                Debug.LogWarning("Can't calculate score for waterInGround");
                break;
            case TypeOfAttribute.Temperature:
                score = GetScoreBasedOnSimpleValue(info.GetTemp());
                break;
            case TypeOfAttribute.PH:
                score = GetScoreBasedOnSimpleValue(info.GetWaterPH());
                break;
            case TypeOfAttribute.Humidity:
                score = GetScoreBasedOnSimpleValue(info.GetHumidity());
                break;
            case TypeOfAttribute.Air:
                score = GetScoreBasedOnSimpleValue(info.GetAir());
                break;
            default:
                Debug.Log("Atribute type is not set --> None Of the above");
                break;
        }
        return (score * AttributesWeeklyScorePercent) / 100f;
    }

    public float GetScoreBasedOnSimpleValue(float value)
    {
        bool isFoundGroup = false;
        int indexInWhichGroup = 0;

        for (int i = 0; i < scoreGroups.Count; i++)
        {
            if (scoreGroups[i].IsInThisGroup(value))
            {
                indexInWhichGroup = i;
                isFoundGroup = true;
                break;
            }
        }

        if (!isFoundGroup)
        {
            Debug.LogWarning("Couldn't find score group for value of: " + value + " of type " + attributeType );
        }

        Debug.Log("The Score For " + name + " For Value Of : " + value  + " Is --> " + scoreGroups[indexInWhichGroup].GetScoreForValue(value));
        return scoreGroups[indexInWhichGroup].GetScoreForValue(value);
    }
}