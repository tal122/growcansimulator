﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfGradeGroup
{
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L
}

[Serializable]
public class SingleScoreGroup : ScriptableObject
{
    [SerializeField] protected new string name;
    [SerializeField] protected TypeOfGradeGroup typeOfGradeGroup;
                     
    //Values Range 
    [SerializeField] protected float minGroupValue;
    [SerializeField] protected float maxGroupValue;
    [SerializeField] protected float worstGroupValue;
    [SerializeField] protected float optimalGroupValue;
                     
    //Grades Range   
    [SerializeField] protected float minGradeValue;
    [SerializeField] protected float maxGradeValue;

    public virtual bool IsInThisGroup(float value)
    {
        if (value >= minGroupValue && value <= maxGroupValue)
        {
            return true;
        }

        return false;
    }

    public virtual bool IsInThisGroup(int value)
    {
        return IsInThisGroup((float)value);
    }
    
    public virtual float GetScoreForValue(float value)
    {
        float valueInverseLerp = Mathf.InverseLerp(worstGroupValue, optimalGroupValue, value);
        float gradeLerp = Mathf.Lerp(minGradeValue, maxGradeValue, valueInverseLerp);
        return gradeLerp;
    }

    public virtual float GetScoreForValue(Fertilizer fertillizer, List<TypeOfFertilizer> typesOfFertilizersToCheck)
    {
        return 0f;
    }

    public float GetScoreForValue(int value)
    {
        return GetScoreForValue((float) value);
    }
}