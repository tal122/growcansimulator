﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamLevelScriptableObject : ScriptableObject
{
    [SerializeField] private int humidityEffect;
    [SerializeField] private int electricConsumePerHour;

    public int GetHumidityEffect()
    {
        return humidityEffect;
    }

    public int GetElectricConsume()
    {
        return electricConsumePerHour;
    }
}