﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirVentaTypeScriptableObject : ScriptableObject {

    [SerializeField] private int humidityEffect;
    [SerializeField] private int temperatureEffect;
    [SerializeField] private int electricConsumePerHour;

    public int GetHumidityEffect()
    {
        return humidityEffect;   
    }

    public int GetTemperatureEffect()
    {
        return temperatureEffect;
    }

    public int GetElectricConsume()
    {
        return electricConsumePerHour;
    }
}