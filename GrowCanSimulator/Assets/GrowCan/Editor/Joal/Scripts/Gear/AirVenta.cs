﻿using UnityEngine;

public class AirVenta : MonoBehaviour
{
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;
    [SerializeField] private bool isOn;
    [SerializeField] private AirVentaTypeScriptableObject VentaType;
    private float totalElectricConsumed = 0;

    private void Start()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    public void Set(bool on)
    {
        if (isOn != on)
        {
            if (on)
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(VentaType.GetHumidityEffect());
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(VentaType.GetTemperatureEffect());
            }
            else
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-VentaType.GetHumidityEffect());
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(-VentaType.GetTemperatureEffect());
            }

            isOn = on;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Set(!isOn);
        }

        if (isOn)
        {
            float electricityAmount = VentaType.GetElectricConsume() * (Time.deltaTime / 3600);

            ConsumeElectricity(electricityAmount);
            myGrowSpaceAttributesInfo.ConsumeElectricity(electricityAmount);
        }
    }

    private void ConsumeElectricity(float amountToConsume)
    {
        totalElectricConsumed += amountToConsume;
    }

    public bool GetIsOn()
    {
        return isOn;
    }
}