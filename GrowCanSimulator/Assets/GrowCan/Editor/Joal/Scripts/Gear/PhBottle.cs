﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhBottle : MonoBehaviour
{
    [Range(-5f,5f)] [SerializeField] private float phEffect;
    [SerializeField] private KeyCode keyToAdd;
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;

    void Start ()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    public void Use()
    {
        myGrowSpaceAttributesInfo.AddWaterPH(phEffect);
    }

	void Update ()
    {
        if (Input.GetKeyDown(keyToAdd))
        {
            Use();
        }
	}
}