﻿using UnityEngine;

public class Shpritzer : MonoBehaviour
{
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;
    [SerializeField] private int humidityEffect = 1;

    [SerializeField]
    [Tooltip("Effect Duration in hours")]
    private float effectDuration = 0.5f;

    void Start()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Use();
        }
    }

    void Use()
    {
       myGrowSpaceAttributesInfo.AddTempHumidityBonus(humidityEffect,effectDuration);
    }

    bool IsCanUse()
    {
        //check if we are in the right week
        return true;
        //return result
    }
}