﻿using UnityEngine;

public class ClosetLight : MonoBehaviour
{
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;

    [Header("Base Settings")]
    [SerializeField] private bool isOn;
    [SerializeField] private int lightHeight;
    private float totalElectricConsumed = 0;

    [Header("Consume and Effect Settings")]
    [SerializeField] private int hoursToConsume;
    [SerializeField] private int humidityEffect;
    [SerializeField] private int temperatureEffect;
    [SerializeField] private int electricConsumePerHour;

    private void Start()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Set(!isOn);
        }

        if (isOn)
        {
            float electricityAmount = electricConsumePerHour * (Time.deltaTime / 3600);

            ConsumeElectricity(electricityAmount); 
            myGrowSpaceAttributesInfo.ConsumeElectricity(electricityAmount);
        }
    }

    private void ConsumeElectricity(float amountToConsume)
    {
        totalElectricConsumed += amountToConsume;
    }

    public void Set(bool on)
    {
        if (isOn != on)
        {
            if (on)
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(humidityEffect);
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(temperatureEffect);
            }
            else
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-humidityEffect);
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(-temperatureEffect);
            }

            isOn = on;
        }
    }

    public bool GetIsOn()
    {
        return isOn;
    }

    public void SetLightHeight(int _height)
    {
        lightHeight = _height;
    }

    public int GetLightHeight()
    {
        return lightHeight;
    }

    public void PressOnSwitch()
    {
        isOn = !isOn;
    }
}