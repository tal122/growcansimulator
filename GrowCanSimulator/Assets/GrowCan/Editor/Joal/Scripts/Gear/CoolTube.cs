﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolTube : MonoBehaviour
{
    [SerializeField] private int temperatureEffect = -4;

    public int GetTemperatureEffect()
    {
        return temperatureEffect;
    }
}