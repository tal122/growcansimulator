﻿using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour
{
    [Header("Fan Settings")]
    [SerializeField] private bool isOn;
     // Move to GrowSpace --> to the closet
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;
    private float totalElectricConsumed = 0;
    private FanEffectGroup currentEffectGroupUsed;
    private FanEffectGroup lastEffectGroupUsed;

    [Header("Consume and Effect Settings")]
    [SerializeField] private int electricConsumePerHour;
    [SerializeField] private List<FanEffectGroup> FanEffectsList;

    private void Start()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Set(!isOn);
        }

        if (isOn)
        {
            float electricityAmount = electricConsumePerHour * (Time.deltaTime / 3600);

            ConsumeElectricity(electricityAmount);
            myGrowSpaceAttributesInfo.ConsumeElectricity(electricityAmount);
        }
    }

    private void ConsumeElectricity(float amountToConsume)
    {
        totalElectricConsumed += amountToConsume;
    }

    public void Set(bool on)
    {
        currentEffectGroupUsed = GetEffectGroupBasedOnDistance();
        if (isOn != on)
        {
            if (on)
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(currentEffectGroupUsed.GetHumidityEffect());
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(currentEffectGroupUsed.GetTemperatureEffect());
                lastEffectGroupUsed = currentEffectGroupUsed;
                //Need to add plant strength
            }
            else
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-lastEffectGroupUsed.GetHumidityEffect());
                myGrowSpaceAttributesInfo.AddConstantTemperatureBonus(-lastEffectGroupUsed.GetTemperatureEffect());
                //Need to add plant strength
            }

            isOn = on;
        }
    }

    public bool GetIsOn()
    {
        return isOn;
    }

    private FanEffectGroup GetEffectGroupBasedOnDistance()
    {
        if (FanEffectsList == null || FanEffectsList.Count == 0)
        {
            Debug.LogError("FanEffectsGroup list is empty --> fix that");
        }

        float distance = CalculateDistanceInCentimetersFromIdealSpot(myGrowSpaceAttributesInfo.GetIdealFanPositionsList());
        foreach (FanEffectGroup fanEffectGroup in FanEffectsList)
        {
            if (fanEffectGroup.IsInThisGroup(distance))
            {
                return fanEffectGroup;
            }
        }

        Debug.LogWarning("Couldn't find a Fan Effect Group that feet the distance");
        return FanEffectsList[0];
    }

    private float CalculateDistanceInCentimetersFromIdealSpot(List<Transform> idealsSpotsList)
    {
        float minDistance;
        if (idealsSpotsList != null && idealsSpotsList.Count > 0)
        {
            minDistance = Vector3.Distance(transform.position, idealsSpotsList[0].position) * 100;
        }
        else
        {
            Debug.LogError("Ideals spots for fan is null or empty --> fix");
            return 0f;
        }

        foreach (var idealSpot in idealsSpotsList)
        {
            float distance = Vector3.Distance(transform.position, idealSpot.position) * 100;

            if (distance < minDistance)
            {
                minDistance = distance;
            }
        }

        return minDistance;
    }
}