﻿using System.Collections.Generic;
using UnityEngine;

public class Steam : MonoBehaviour
{
    private GrowSpaceAttributesInfo myGrowSpaceAttributesInfo;
    [SerializeField] private int steamLevel = 0;
    [SerializeField] private int lastSteamLevel = -1;
    [SerializeField] private bool isOn;
    [SerializeField] private List<SteamLevelScriptableObject> SteamLevels;
    private float totalElectricConsumed = 0;

    private void Start()
    {
        myGrowSpaceAttributesInfo = GetComponentInParent<GrowSpaceAttributesInfo>();

        if (myGrowSpaceAttributesInfo == null)
        {
            Debug.LogError("Couldn't find GrowSpaceAttribute Component in parent");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Set(!isOn,steamLevel);

        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Set(isOn, 0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Set(isOn, 1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Set(isOn, 2);
        }

        if (isOn)
        {
            float electricityAmount = GetSteamLevel().GetElectricConsume() * (Time.deltaTime / 3600);

            ConsumeElectricity(electricityAmount);
            myGrowSpaceAttributesInfo.ConsumeElectricity(electricityAmount);
        }
    }

    public void Set(bool on,int level)
    {
        if (level < 0)
        {
            Debug.LogWarning("New Steam Work level is below 0 --> fix that");
            return;
        }

        SteamLevelScriptableObject currentSteamLevel = GetSteamLevel(); 

        if(steamLevel != level)
        {
            lastSteamLevel = steamLevel;
            steamLevel = level;
            steamLevel = Mathf.Clamp(steamLevel, 0, SteamLevels.Count);
            currentSteamLevel = GetSteamLevel();

            if (isOn && on)
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-SteamLevels[lastSteamLevel].GetHumidityEffect());
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(currentSteamLevel.GetHumidityEffect());
            }
        }

        if (isOn != on)
        {
            if (on)
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(currentSteamLevel.GetHumidityEffect());
            }
            else
            {
                myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-currentSteamLevel.GetHumidityEffect());
            }

            isOn = on;
        }
    }

    public bool GetIsOn()
    {
        return isOn;
    }

    public int GetWorkLevel()
    {
        return steamLevel;
    }

    public void SetSteamLevelNumber(int _workLevel)
    {
      
        myGrowSpaceAttributesInfo.AddConstantHumidityBonus(-GetSteamLevel().GetHumidityEffect());
        steamLevel = _workLevel;
        steamLevel = Mathf.Clamp(steamLevel, 0, SteamLevels.Count);
    }

    public int GetSteamLevelNumber(int _workLevel)
    {
        return steamLevel;
    }

    public SteamLevelScriptableObject GetSteamLevel()
    {
        return SteamLevels[steamLevel];
    }

    private void ConsumeElectricity(float amountToConsume)
    {
        totalElectricConsumed += amountToConsume;
    }
}