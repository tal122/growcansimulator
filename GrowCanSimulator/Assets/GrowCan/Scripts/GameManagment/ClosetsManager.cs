﻿using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using UnityEngine;

public class ClosetsManager : MonoBehaviour
{
    private ClosetHandler[] closetHandlers;
    [HideInInspector] public ClosetStateEvent EventStateChanged;

    private void Awake()
    {
        closetHandlers = GetComponentsInChildren<ClosetHandler>();
        EventStateChanged = new ClosetStateEvent();
    }

    private void Start()
    {
        foreach (ClosetHandler closetHandler in closetHandlers)
        {
            closetHandler.EventClosetEventHandler.AddListener(UpdateClosetChanged);
        }
    }

    /// <summary>
    /// Update others that the closets changed
    /// </summary>
    /// <param name="closetState"></param>
    public void UpdateClosetChanged(ClosetState closetState)
    {
        EventStateChanged.Invoke(closetState);
    }

    /// <summary>
    /// Update the closet to the new states from the server
    /// </summary>
    /// <param name="closetStates"></param>
    public void UpdateClosetStates(ClosetState[] closetStates)  //always same closet
    {
        if (closetStates != null && closetHandlers != null)
        {
            foreach (ClosetHandler closetHandler in closetHandlers) //bad complexity but its only small number of closets
            {
                if (closetHandler.ClosetNumber == closetStates[0].ClosetNumber)
                {
                    closetHandler.UpdateClosetStates(closetStates);
                    break;
                }
            }
        }
        else
        {
            Debug.LogError("No next states or no closets");
        }
    }

    /// <summary>
    /// Sets the closets data from the input
    /// </summary>
    /// <param name="currentClosetsState"></param>
    public void SetClosetsState(ClosetState[] currentClosetsState) 
    {
        if (currentClosetsState != null)
        {
            foreach (ClosetState closetState in currentClosetsState)
            {
                for (int i = 0; i < closetHandlers.Length; i++) //bad complexity but its only small number of closets
                {
                    if (closetHandlers[i].ClosetNumber == closetState.ClosetNumber)
                    {
                        closetHandlers[i].SetClosetNextState(closetState);
                        break;
                    }
                }
            }
        }
        else
        {
            Debug.Log("Closet state is null!");
        }
    }

    public void PrintClosetsState()
    {
        foreach (ClosetHandler closetHandler in closetHandlers)
        {
            closetHandler.PrintClosetState();
        }
    }
}