﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //[SerializeField] private ServerHandler mockServer;
    //[SerializeField] private ClosetsManager closetsManager;
    //private ClosetStateEvent eventStateHandler;

    private void Awake()
    {
        //eventStateHandler = new ClosetStateEvent();
    }

    private void Start()
    {
        //closetsManager.EventStateChanged.AddListener(UpdateServer);
        //mockServer.ConnectToServer();
        //closetsManager.SetClosetsState(mockServer.GetInitialState());
        //closetsManager.PrintClosetsState();
    }

    private void Update()
    {
        if (Input.GetKey("escape"))
        {
            ExitGame();
        }
    }
    
    public void RestartScene()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void UpdateClosetsStates(ClosetState[] closetStates)
    {
        //closetsManager.UpdateClosetStates(closetStates);
    }

    private void UpdateServer(ClosetState closetState)
    {
        //mockServer.UpdateFakeClosetsStates(closetState);
    }
}