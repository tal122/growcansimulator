﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ClosetState
{
    public TimeFormat timeStamp;
    public int ClosetNumber;
    //public float Temperature;
    //public float MoisturizerAir;
    //public float SoilMoisturizer;
    //public float Fertilizer;
    //public float Water100mil;
    //public float Ventilation;
    //public float Humidifier;
    //public float CoolTube;
    //public float Vent; //airDisposal
    //public float Sprinkler;
    public bool IsLightOn;
    //public float LightHight;
    public bool IsFanOn;
    //public float FanCirculation;
    public float SizeOfPlant;
}