﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eCameraCanvasType
{
    game,
    newGame,
    chooseName,
    ChooseCannabisStrain,
    SetRoomGeneralInfo, 
    Store
}

public class CanvasCameraHandler : MonoBehaviour
{
    [SerializeField] private bool isStartGame;
    [Header("Cameras")]
    [SerializeField] private Camera cameraMenu;
    [SerializeField] private Camera cameraGame;
    [SerializeField] private Camera cameraStore;
    [Header("Canvas and UI")]
    [SerializeField] private GameObject canvasMenu;
    [SerializeField] private GameObject canvasGame;
    [SerializeField] private GameObject canvasStore;
    [SerializeField] private GameObject newGameUI;
    [SerializeField] private GameObject chooseNameUI;
    [SerializeField] private GameObject chooseCannabisStrain;
    [SerializeField] private GameObject SetRoomGeneralInfo;

    private void Start()
    {
        if (isStartGame == true)
        {
            SetCanvasCamera((int)eCameraCanvasType.game);
        }
        else
        {
            SetCanvasCamera((int)eCameraCanvasType.newGame);
        }
    }

    public void SetCanvasCamera(int cameraCanvasType)
    {
        EnableAllCanvasMenuUI(false);
        EnableAllCanvasCamera(false);
        EnableStoreCanvasAndCamera(false);
        switch ((eCameraCanvasType)cameraCanvasType)
        {
            case eCameraCanvasType.newGame:
                {
                    EnableMenuCanvasAndCamera(true);
                    newGameUI.gameObject.SetActive(true);
                    break;
                }
            case eCameraCanvasType.chooseName:
                {
                    EnableMenuCanvasAndCamera(true);
                    chooseNameUI.gameObject.SetActive(true);
                    break;
                }
            case eCameraCanvasType.ChooseCannabisStrain:
                {
                    EnableMenuCanvasAndCamera(true);
                    chooseCannabisStrain.gameObject.SetActive(true);
                    break;
                }
            case eCameraCanvasType.SetRoomGeneralInfo:
                {
                    EnableMenuCanvasAndCamera(true);
                    SetRoomGeneralInfo.gameObject.SetActive(true);
                    break;
                }
            case eCameraCanvasType.game:
                {
                    EnableGameCanvasAndCamera(true);
                    break;
                }
            case eCameraCanvasType.Store:
                {
                    EnableStoreCanvasAndCamera(true);
                    break;
                }
            default:
                break;
        }
    }

    private void EnableGameCanvasAndCamera(bool isEnable)
    {
        canvasGame.gameObject.SetActive(isEnable);
        cameraGame.gameObject.SetActive(isEnable);
    }

    private void EnableMenuCanvasAndCamera(bool isEnable)
    {
        cameraMenu.gameObject.SetActive(isEnable);
        canvasMenu.gameObject.SetActive(isEnable);
    }

    private void EnableStoreCanvasAndCamera(bool isEnable)
    {
        canvasStore.gameObject.SetActive(isEnable);
        cameraStore.gameObject.SetActive(isEnable);
    }

    private void EnableAllCanvasMenuUI(bool isEnable)
    {
        newGameUI.gameObject.SetActive(isEnable);
        chooseNameUI.gameObject.SetActive(isEnable);
        chooseCannabisStrain.gameObject.SetActive(isEnable);
        SetRoomGeneralInfo.gameObject.SetActive(isEnable);
    }

    private void EnableAllCanvasCamera(bool isEnable)
    {
        EnableMenuCanvasAndCamera(isEnable);
        EnableGameCanvasAndCamera(isEnable);
        EnableStoreCanvasAndCamera(isEnable);
    }
}