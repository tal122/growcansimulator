﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsSystemHandler : MonoBehaviour
{
    private int points;
    public int Points
    {
        get { return points; }
        set { points = value; }
    }

    [SerializeField] private int startingPoints;

    private void Awake()
    {
        points = startingPoints;
    }

    public void AddPoints(int value)
    {
        points = value;
    }
}