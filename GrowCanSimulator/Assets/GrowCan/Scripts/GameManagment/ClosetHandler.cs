﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetHandler : MonoBehaviour
{
    public int ClosetNumber { get; set; }
    private List<ClosetState> nextClosetsState;
    [HideInInspector] public EventClosetHandler EventClosetEventHandler;
    #region properties
    [SerializeField] private TimeUtility timeHandler;
    [SerializeField] private BoolSystem lightSystem;
    [SerializeField] private BoolSystem fanSystem;
    //[SerializeField] private FloatSystem plantSizeSystem;

    //[SerializeField] private TemperatureSystem temperatureSystem;
    //[SerializeField] private MoisturizerAirSystem moisturizerAirSystem;
    //[SerializeField] private MoisturizerSoilSystem soisturizerSoilSystem;
    //[SerializeField] private FertilizerSystem fertilizerSystem;
    //[SerializeField] private Water100milSystem waterSystem;
    //[SerializeField] private FanCirculationSystem fanCirculationSystem;
    //[SerializeField] private HumidifierSystem humidifierSystem;
    //[SerializeField] private CoolTubeSystem coolTubeSystem;
    //[SerializeField] private VentSystem ventSystem;
    //[SerializeField] private SprinklerSystem sprinklerSystem;
    //[SerializeField] private LightHightSystem lightHightSystem;
    #endregion

    private void Awake()
    {
        EventClosetEventHandler = new EventClosetHandler();
        nextClosetsState = new List<ClosetState>();
    }

    private void Start()
    {
        RegisterAllToOnValueChanged();
    }

    private void RegisterAllToOnValueChanged()
    {
        //lightSystem.SwitchHandling.ValueChangedEvent.AddListener(UpdateClosetChanged);
        //fanSystem.SwitchHandling.ValueChangedEvent.AddListener(UpdateClosetChanged);
        //plantSizeSystem.ValueAdjuster.ValueChangedEvent.AddListener(UpdateClosetChanged);
    }

    public void UpdateClosetChanged()
    {
        EventClosetEventHandler.Invoke(GetClosetState());
    }

    /// <summary>
    /// Update the closet to the new states from the server
    /// </summary>
    /// <param name="closetStates"></param>
    public void UpdateClosetStates(ClosetState[] closetStates)
    {
        if (nextClosetsState == null)
        {
            nextClosetsState = new List<ClosetState>();
        }

        nextClosetsState.AddRange(closetStates);
    }

    public void SetClosetNextState(ClosetState closetState)
    {
        //float timeToNextValue = timeHandler.GetTimeDifferenceInSeconds(closetState.timeStamp);
        //lightSystem.SwitchHandling.SetNextValue(closetState.IsLightOn, timeToNextValue);
        //fanSystem.SwitchHandling.SetNextValue(closetState.IsFanOn, timeToNextValue);
        //plantSizeSystem.ValueAdjuster.SetNextValue(closetState.SizeOfPlant, timeToNextValue);
    }

    public ClosetState GetClosetState()
    {
        ClosetState closetState = new ClosetState();
        //closetState.timeStamp = timeHandler.GetTime();
        //closetState.IsLightOn = lightSystem.SwitchHandling.Value;
        //closetState.IsFanOn = fanSystem.SwitchHandling.Value;
        //closetState.SizeOfPlant = plantSizeSystem.ValueAdjuster.Value;
        return closetState;
    }

    public void PrintClosetState()
    {
        //Debug.Log("ClosetNumber: " + ClosetNumber.ToString() + "\n" + temperatureSystem.ToString()
        //    + "\n" + moisturizerAirSystem.ToString() + "\n" + fertilizerSystem.ToString()
        //    + "\n" + waterSystem.ToString() + "\n" + ventilationSystem.ToString()
        //    + "\n" + humidifierSystem.ToString() + "\n" + coolTubeSystem.ToString()
        //    + "\n" + ventSystem.ToString() + "\n" + sprinklerSystem.ToString()
        //    + "\n" + lightSystem.ToString() + "\n" + fanSystem.ToString());
    }
}