﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ClosetStateFloatLevels : MonoBehaviour, IValueChangeableFloat
{
    [SerializeField] private Text closetStateText;
    [SerializeField] private string initialPrefexText;
    [SerializeField] private GameObject collidersOnClickHandlerGameObject;
    [SerializeField] private IOnObjectClickBase<float> onClickChangeLevels;
    public FloatEvent OnValueChanged { get; set; }
    private float value;
    public float Value
    {
        get
        {
            return value;
        }
        set
        {
            this.value = value;
            if (OnValueChanged != null)
            {
                OnValueChanged.Invoke(value);
            }

            UpdateUIText(value.ToString());
        }
    }

    private void Awake()
    {
        onClickChangeLevels = collidersOnClickHandlerGameObject.GetComponent<IOnObjectClickBase<float>>();
    }

    private void Start()
    {
        if (closetStateText != null)
        {
            initialPrefexText = closetStateText.text;
        }

        onClickChangeLevels.AddListener(UpdateValueHandling);
        UpdateUIText(Value.ToString());
    }

    private void UpdateValueHandling(float value)
    {
        Value = value;
    }

    private void UpdateUIText(string text)
    {
        closetStateText.text = initialPrefexText + text;
    }
}