﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoolSystem : MonoBehaviour, IToggleable
{
    [SerializeField] private GameObject[] effectedGameObjects;
    public ValueHandlingBool SwitchHandling { get; set; }
    public BoolEvent OnToggleChanged { get; set; }
    [SerializeField] GameObject collidersOnClickHandlerGameObject;
    private IOnObjectClickBase<bool> onClickChangeLevels;
    private bool isOn;
    public bool IsOn
    {
        get { return SwitchHandling.Value; }
        set { SwitchHandling.SetNextValue(value, 0); }
    }

    private void Awake()
    {
        onClickChangeLevels = collidersOnClickHandlerGameObject.GetComponent<IOnObjectClickBase<bool>>();
        if (OnToggleChanged == null)
        {
            OnToggleChanged = new BoolEvent();
        }

        if (SwitchHandling == null)
        {
            SwitchHandling = new ValueHandlingBool();
        }

        if (SwitchHandling.OnValueChanged == null)
        {
            SwitchHandling.OnValueChanged = new BoolEvent();
        }

        SwitchHandling.OnValueChanged.AddListener(UpdateOnValueChanged);
        onClickChangeLevels.AddListener(SetValue);
    }

    public void UpdateOnValueChanged(bool isValue)
	{
        if (OnToggleChanged != null)
        {
            OnToggleChanged.Invoke(isValue);
	    }
    }
    
    public void Toggle()
    {
        if (SwitchHandling == null)    //TODO: how??? why is this ignoring the if, LightSwitch is clearly not null
        {
            SwitchHandling.Toggle();
            SwitchHandling.ActivateEffectedGameObjects(effectedGameObjects);
        }
    }

    public void SetValue(bool isValue)
    {
        if (SwitchHandling == null)    //TODO: how??? why is this ignoring the if, LightSwitch is clearly not null
        {
            SwitchHandling.SetBoolValue(isValue);
            SwitchHandling.ActivateEffectedGameObjects(effectedGameObjects);
        }
    }
}