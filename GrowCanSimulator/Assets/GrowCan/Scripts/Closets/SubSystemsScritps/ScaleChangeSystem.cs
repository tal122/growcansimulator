﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleChangeSystem : MonoBehaviour
{
    [SerializeField] private GameObject subSystem;
    private IValueChangeableFloat subsystem;
    [SerializeField] private Vector3 valueModifier;

    #region Objects effected
    [SerializeField] private GameObject[] sizeChangedGameObjects;
    private int numberOfSizeChangedGameObjects;
    private Vector3[] originalSizesOfChangedGameObjects;
    #endregion

    private void Awake()
    {
        numberOfSizeChangedGameObjects = sizeChangedGameObjects.Length;
        originalSizesOfChangedGameObjects = new Vector3[numberOfSizeChangedGameObjects];
    }

    private void Start()
    {
        subsystem = subSystem.GetComponent<IValueChangeableFloat>();

        if (subsystem.OnValueChanged == null)
        {
            subsystem.OnValueChanged = new FloatEvent();
        }

        subsystem.OnValueChanged.AddListener(ChangeSize);

        for (int i = 0; i < numberOfSizeChangedGameObjects; i++)
        {
            originalSizesOfChangedGameObjects[i] = sizeChangedGameObjects[i].transform.localScale;
        }
    }
    
    private void ChangeSize(float Value)
    {
        for (int i = 0; i < numberOfSizeChangedGameObjects; i++)
        {
            sizeChangedGameObjects[i].transform.localScale = Value *
                new Vector3(originalSizesOfChangedGameObjects[i].x * valueModifier.x,
                            originalSizesOfChangedGameObjects[i].y * valueModifier.y,
                            originalSizesOfChangedGameObjects[i].z * valueModifier.z);
        }
    }
}