﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubSystemFloatValue : MonoBehaviour, IValueChangeableFloat
{
    [SerializeField] private float startingValue;
    public FloatEvent OnValueChanged { get; set; }
    private float value;
    public float Value
    {
        get
        {
            return this.value;
        }
        set
        {
            this.value = value;
            if (OnValueChanged != null)
            {
                OnValueChanged.Invoke(this.value);
            }
        }
    }

    private void Start()
    {
        Value = startingValue;
    }
}