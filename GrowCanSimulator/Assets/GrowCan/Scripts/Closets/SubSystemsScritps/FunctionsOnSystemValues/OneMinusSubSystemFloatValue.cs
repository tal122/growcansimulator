﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneMinusSubSystemFloatValue : SubSystemFloatValue
{

    [SerializeField] private GameObject subSystem;
    private IValueChangeableFloat subsystem;
    
    private void Start()
    {
        subsystem = subSystem.GetComponent<IValueChangeableFloat>();

        if (subsystem.OnValueChanged == null)
        {
            subsystem.OnValueChanged = new FloatEvent();
        }

        subsystem.OnValueChanged.AddListener(UpdateSystemValue);
    }

    private void UpdateSystemValue(float value) //TODO: useless input value
    {
        Value = 1 - value;
    }
}