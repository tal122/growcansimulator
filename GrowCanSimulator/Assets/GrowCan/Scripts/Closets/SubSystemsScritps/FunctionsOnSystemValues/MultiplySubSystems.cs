﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplySubSystems : SubSystemFloatValue
{
    [SerializeField] private GameObject[] subSystems;
    private IValueChangeableFloat[] subsystems;

    private void Awake()
    {
        for (int i = 0; i < subSystems.Length; i++)
        {
            subsystems[i] = subSystems[i].GetComponent<IValueChangeableFloat>();
        }
    }

    private void Start()
    {
        if (subsystems != null)
        {
            for (int i = 0; i < subsystems.Length; i++)
            {
                if (subsystems[i].OnValueChanged == null)
                {
                    subsystems[i].OnValueChanged = new FloatEvent();
                }

                subsystems[i].OnValueChanged.AddListener(UpdateSystemValue);

            }
        }
    }
    
    private void UpdateSystemValue(float value) //TODO: useless input value
    {
        float tempValue = 1;
        foreach (IValueChangeableFloat system in subsystems)
        {
            tempValue *= system.Value;
        }

        Value = tempValue;
    }
}
