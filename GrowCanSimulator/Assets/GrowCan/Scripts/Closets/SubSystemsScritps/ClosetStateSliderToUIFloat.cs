﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClosetStateSliderToUIFloat : ValueHandlingFloat
{
    [SerializeField] private Text closetStateText;
    [SerializeField] private string initialPrefexText;
    [SerializeField] private UISliderHandler firstSliderable, secondSliderable;
    [SerializeField] private Transform systemPosition1, systemPosition2;

    private void Start()
    {
        initialPrefexText = closetStateText.text;
        Value = Vector3.Distance(systemPosition1.position, systemPosition2.position);
        RegisterToEvents();
        UpdateClosetStateText(0);
    }

    private void RegisterToEvents()
    {
        if (firstSliderable != null)
        {
            if (firstSliderable.OnValueChanged == null)
            {
                firstSliderable.OnValueChanged = new FloatEvent();
            }

            firstSliderable.OnValueChanged.AddListener(UpdateClosetStateText);
        }

        if (secondSliderable != null)
        {
            if (secondSliderable.OnValueChanged == null)
            {
                secondSliderable.OnValueChanged = new FloatEvent();
            }

            secondSliderable.OnValueChanged.AddListener(UpdateClosetStateText);
        }
    }

    private void UpdateClosetStateText(float value) //TODO: shouldn't be an input with no use
    {
        Value = Vector3.Distance(systemPosition1.position, systemPosition2.position);
        closetStateText.text = initialPrefexText + Value.ToString("F2");
    }
}