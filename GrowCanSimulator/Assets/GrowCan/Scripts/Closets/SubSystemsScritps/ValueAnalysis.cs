﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueAnalysis : ValueHandlingFloat
{

    [SerializeField] float lessThanMinValue, lessThanTimer, minRange, inRangeValue, inRangeTimer, maxRange, moreThenMaxValue, moreThanTimer;
    [SerializeField] private GameObject observedSystem;
    [SerializeField] IValueChangeableFloat observedObject;
    private IEnumerator changeValueCoroutine;
    private float lastValue;

    private void Awake()
    {
        if (observedSystem != null)
        {
            observedObject = observedSystem.GetComponent<IValueChangeableFloat>();
        }
    }

    private void Start()
    {
        if (observedObject.OnValueChanged == null)
        {
            observedObject.OnValueChanged = new FloatEvent();
        }

        observedObject.OnValueChanged.AddListener(AnalyseValue);
    }

    private void AnalyseValue(float value)
    {
        float timerToWait = 0, valueToChange = 404;
        bool isCallCoroutine = false;
        if (value < minRange && lastValue >= minRange)
        {
            isCallCoroutine = true;
            timerToWait = lessThanTimer;
            valueToChange = lessThanMinValue;
        }
        else if (value > maxRange && lastValue <= maxRange)
        {
            isCallCoroutine = true;
            timerToWait = inRangeTimer;
            valueToChange = moreThenMaxValue;
        }
        else if (value >= minRange && value <= maxRange && (lastValue >= maxRange || lastValue < minRange))
        {
            isCallCoroutine = true;
            timerToWait = inRangeTimer;
            valueToChange = inRangeValue;
        }

        if (isCallCoroutine == true)
        {
            if (changeValueCoroutine != null)
            {
                StopCoroutine(changeValueCoroutine);
            }

            changeValueCoroutine = WairForXSeconds(timerToWait, valueToChange);
            StartCoroutine(changeValueCoroutine);
        }
        
        lastValue = value;
    }

    IEnumerator WairForXSeconds(float secondsToWait, float value)
    {
        yield return new WaitForSeconds(secondsToWait);
        Value = value;
    }
}