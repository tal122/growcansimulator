﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetSubSystemReverseValue : MonoBehaviour, IValueChangeableFloat
{
    [SerializeField] private ValueHandlingFloat reversibleSubsystem;
    [SerializeField] private Transform systemPosition1, systemPosition2;

    private float minMaxDistance;
    private float value;
    public float Value
    {
        get
        {
            return value;
        }
        private set
        {
            this.value = minMaxDistance - value;
            if (OnValueChanged != null)
            {
                OnValueChanged.Invoke(this.value);
            }
        }
    }

    private FloatEvent onValueChanged;
    public FloatEvent OnValueChanged
    {
        get
        {
            return onValueChanged;
        }

        set
        {
            onValueChanged = value;
        }
    }
    
    private void Start()
    {
        if (reversibleSubsystem != null)
        {
            minMaxDistance = Vector3.Distance(systemPosition1.position, systemPosition2.position);
            if (reversibleSubsystem.OnValueChanged == null)
            {
                reversibleSubsystem.OnValueChanged = new FloatEvent();
            }

            reversibleSubsystem.OnValueChanged.AddListener(SetValue);
        }
    }

    private void SetValue(float value)
    {
        Value = value;
    }
}