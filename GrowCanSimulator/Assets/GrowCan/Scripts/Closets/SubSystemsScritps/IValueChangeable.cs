﻿using UnityEngine;
using UnityEngine.Events;

public interface IValueChangeable
{}

public interface IValueChangeableFloat : IValueChangeable
{
    FloatEvent OnValueChanged { get; set; }
    float Value { get; }
}

public interface IValueChangeableBool: IValueChangeable
{
    BoolEvent OnValueChanged { get; set; }
    bool IsValue { get; }
}