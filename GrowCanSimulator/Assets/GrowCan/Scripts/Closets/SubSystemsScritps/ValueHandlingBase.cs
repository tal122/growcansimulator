﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ValueHandlingBase<T> : MonoBehaviour
{
    [SerializeField] protected Text UITextValue;
    [SerializeField] protected string prefaxUIText;

    private T nextValue;
    public T NextValue
    {
        get
        {
            return nextValue;
        }
        set
        {
            nextValue = value;
            startOfLerpValue = Value;
            lerpTimeCounter = 0;
        }
    }

    protected float lerpTimeCounter;
    protected T startOfLerpValue;
    public float timeToLerpInSeconds { get; set; }   //Needs to come from the server
    private T value;
    public T Value
    {
        get
        {
            return value;
        }
        protected set
        {
            this.value = value;
            ValueChanged(this.value);
            UpdateUIText();
        }
    }

    protected virtual void ValueChanged(T value) { }

    public void SetNextValue(T nextValue, float timeToLerpInSeconds)
    {
        if (timeToLerpInSeconds == 0)
        {
            Value = nextValue;
        }
        else
        {
            this.NextValue = nextValue;
            this.timeToLerpInSeconds = timeToLerpInSeconds;
        }
    }

    protected virtual void UpdateUIText()
    {
        if (UITextValue != null)
        {
            UITextValue.text = prefaxUIText + Value.ToString();
        }
    }
}