﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetSystemManager : ValueHandlingFloat
{
    [SerializeField] private float startingValue;
    [Header("Subsystems")]
    [SerializeField] private float[] subSystemsHandlerCoefficients;
    [SerializeField] private GameObject[] subsystems;
    private IValueChangeableFloat[] subSystemsHandler;
    [Header("Systems")]
    [SerializeField] private float[] systemsHandlerCoefficients;
    [SerializeField] private GameObject[] systems;
    private IValueChangeableFloat[] systemsHandler;
    [SerializeField] private float timeToUpdateToNewValues;

    private void Awake()
    {
        subSystemsHandler = new IValueChangeableFloat[subsystems.Length];
        for (int i = 0; i < subsystems.Length; i++)
        {
            subSystemsHandler[i] = subsystems[i].GetComponent<IValueChangeableFloat>();
        }

        systemsHandler = new IValueChangeableFloat[systems.Length];
        for (int i = 0; i < systems.Length; i++)
        {
            systemsHandler[i] = systems[i].GetComponent<IValueChangeableFloat>();
        }
    }

    private void Start()
    {
        RegisterToSubSystemEvents();
        SetNextValue(startingValue, 0);
    }

    private void RegisterToSubSystemEvents()
    {
        int i;
        for (i = 0; i < subSystemsHandler.Length; i++)
        {
            if (subSystemsHandler[i].OnValueChanged == null)
            {
                subSystemsHandler[i].OnValueChanged = new FloatEvent();
            }

            subSystemsHandler[i].OnValueChanged.AddListener(UpdatedValueFromSystems);
        }

        for (i = 0; i < systemsHandler.Length; i++)
        {
            if (systemsHandler[i].OnValueChanged == null)
            {
                systemsHandler[i].OnValueChanged = new FloatEvent();
            }

            systemsHandler[i].OnValueChanged.AddListener(UpdatedValueFromSystems);
        }
    }

    private void UpdatedValueFromSystems(float value)    //TODO: Change, useless input
    {
        float newValue = startingValue;
        for (int i = 0; i < subSystemsHandler.Length; i++)
        {
            newValue += subSystemsHandler[i].Value * subSystemsHandlerCoefficients[i];
        }

        for (int i = 0; i < systemsHandler.Length; i++)
        {
            newValue += systemsHandler[i].Value * systemsHandlerCoefficients[i];
        }

        SetNextValue(newValue, 5);
    }
}