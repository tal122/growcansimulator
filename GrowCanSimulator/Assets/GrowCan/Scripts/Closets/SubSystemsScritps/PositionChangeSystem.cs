﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PositionChangeSystem : MonoBehaviour
{
    [SerializeField] private GameObject subSystem;
    private IValueChangeableFloat subsystem;

    private void Start()
    {
        subsystem = subSystem.GetComponent<IValueChangeableFloat>();

        if (subsystem.OnValueChanged == null)
        {
            subsystem.OnValueChanged = new FloatEvent();
        }

        subsystem.OnValueChanged.AddListener(ChangeSize);
    }

    #region Object effected
    [SerializeField] private GameObject positionChangedGameObject;
    [SerializeField] private Transform minTransform, maxTransform;
    private Vector3 positionOffsetMagnitude;
    #endregion

    private void ChangeSize(float value)
    {
        positionChangedGameObject.transform.position = (maxTransform.position - minTransform.position) * value + minTransform.position;
    }
}