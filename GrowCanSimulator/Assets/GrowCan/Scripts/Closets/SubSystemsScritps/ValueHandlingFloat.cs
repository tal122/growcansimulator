﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueHandlingFloat : ValueHandlingBase<float>, IValueChangeableFloat
{
    public FloatEvent OnValueChanged { get; set; }

    protected override void ValueChanged(float value)
    {
        if (OnValueChanged != null)
        {
            OnValueChanged.Invoke(value);   //TODO: suspected of doing a Value set in Venta, causeing stack overflow
        }
    }

    private void Update()
    {
        lerpTimeCounter += Time.deltaTime;
        if (lerpTimeCounter < timeToLerpInSeconds)
        {
            Value = Mathf.Lerp(startOfLerpValue, NextValue, lerpTimeCounter / timeToLerpInSeconds);
        }
    }

    protected override void UpdateUIText()
    {
        if (UITextValue != null)
        {
            UITextValue.text = prefaxUIText + Value.ToString("F2");
        }
    }
}