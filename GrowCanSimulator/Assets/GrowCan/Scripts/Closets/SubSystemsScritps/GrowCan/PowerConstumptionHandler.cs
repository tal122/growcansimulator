﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerConstumptionHandler : MonoBehaviour
{
    [SerializeField] public PowerConsumption PowerConsumption;

    [SerializeField] private GameObject switchSubSystem;
    private IShutOffable shutOffableSubSystem;
    
    [SerializeField] private GameObject valueSubSystem;
    public IValueChangeableFloat valueChangeableSubSystem;

    private void Awake()
    {
        if (switchSubSystem != null)
        {
            shutOffableSubSystem = switchSubSystem.GetComponent<IShutOffable>();
        }

        if (valueSubSystem != null)
        {
            valueChangeableSubSystem = valueSubSystem.GetComponent<IValueChangeableFloat>();
        }
    }

    private void Start()
    {
        RegisterToEvents();
    }

    public void RegisterToEvents()
    {
        if (valueChangeableSubSystem != null)
        {
            if (valueChangeableSubSystem.OnValueChanged == null)
            {
                valueChangeableSubSystem.OnValueChanged = new FloatEvent();
            }

            valueChangeableSubSystem.OnValueChanged.AddListener(OnSubSystemValueChanged);
        }

        if (shutOffableSubSystem != null)
        {
            if (shutOffableSubSystem.BoolSystemHandler.OnToggleChanged == null)
            {
                shutOffableSubSystem.BoolSystemHandler.OnToggleChanged = new BoolEvent();
            }

            shutOffableSubSystem.BoolSystemHandler.OnToggleChanged.AddListener(OnSubSystemSwitchChanged);
        }

        if (PowerConsumption != null)
        {
            if (valueChangeableSubSystem == null)
            {
                EnablePowerConsumptionScript(shutOffableSubSystem.BoolSystemHandler.IsOn);
            }
            else
            {
                OnSubSystemValueChanged(valueChangeableSubSystem.Value);
            }
        }
    }

    private void OnSubSystemSwitchChanged(bool isValue)
    {
        if (isValue == false)
        {
            EnablePowerConsumptionScript(false);
        }
        else
        {
            if (valueChangeableSubSystem == null || valueChangeableSubSystem != null && valueChangeableSubSystem.Value != 0)
            {
                EnablePowerConsumptionScript(true);
            }
        }
    }

    private void OnSubSystemValueChanged(float value)
    {
        if(valueChangeableSubSystem != null && shutOffableSubSystem.BoolSystemHandler.IsOn == true && value != 0)
        {
            EnablePowerConsumptionScript(true);
        }
        else
        {
            EnablePowerConsumptionScript(false);
        }
    }

    private void EnablePowerConsumptionScript(bool isEnabled)
    {
        if (PowerConsumption != null)
        {
            PowerConsumption.enabled = isEnabled;
        }
    }
}