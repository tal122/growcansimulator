﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerConsumption : MonoBehaviour
{
    [HideInInspector] public FloatEvent OnVoltCost;
    [SerializeField] private float voltsCost;
    public float VoltsCost
    {
        get { return voltsCost; }
        set { voltsCost = value; }
    }

    private float secondCheck;
    private void Update()
    {
        secondCheck += Time.deltaTime;
        if (secondCheck > 1)
        {
            if (OnVoltCost != null)
            {
                OnVoltCost.Invoke(VoltsCost);
            }

            secondCheck -= 1;
        }
     }
}