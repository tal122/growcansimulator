﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeColorsHandler : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Material[] colors = new Material[2];
    [SerializeField] private GameObject observedSystem;
    private IValueChangeableFloat observedObject;

    private void Awake()
    {
        if (observedSystem != null)
        {
            observedObject = observedSystem.GetComponent<IValueChangeableFloat>();
        }
    }
        
    private void Start()
    {
        if (observedObject.OnValueChanged == null)
        {
            observedObject.OnValueChanged = new FloatEvent();
        }

        observedObject.OnValueChanged.AddListener(UpdateLeavesColor);
    }

    /// <summary>
    /// Input should be between 0 to 1
    /// </summary>
    /// <param name="value"></param>
    private void UpdateLeavesColor(float value)
    {
        if (value >= 0 && value <= 1)
        {
            meshRenderer.materials[1].color = Color.Lerp(colors[0].color, colors[1].color, value);
        }
    }
}