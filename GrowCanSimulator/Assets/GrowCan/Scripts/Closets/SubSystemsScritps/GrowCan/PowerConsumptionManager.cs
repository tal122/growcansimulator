﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerConsumptionManager : MonoBehaviour
{
    private float totalPowerConsumption;
    [SerializeField] private string initialTextUI;
    [SerializeField] private Text TextUI;
    [SerializeField] private PowerConsumption[] closetSubSystemHandler;

    private void Awake()
    {
        if (TextUI != null)
        {
            initialTextUI = TextUI.text;
        }

        totalPowerConsumption = 0;
    }

    private void Start()
    {
        foreach (PowerConsumption subSystemHandler in closetSubSystemHandler)
        {
            subSystemHandler.OnVoltCost.AddListener(UpdateUIText);
        }

        UpdateUIText(0);
    }

    private void UpdateUIText(float value)
    {
        totalPowerConsumption += value;
        if (TextUI != null)
        {
            TextUI.text = initialTextUI + totalPowerConsumption.ToString("F2");
        }
    }
}