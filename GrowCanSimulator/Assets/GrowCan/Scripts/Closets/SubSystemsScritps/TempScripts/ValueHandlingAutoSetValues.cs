﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueHandlingAutoSetValues : MonoBehaviour
{
    [SerializeField] private GameObject system;
    private ValueHandlingBase<float> systemFloat;
    [SerializeField] private float startingVextValue, nextValue, time;

    private void Awake()
    {
        systemFloat = system.GetComponent<ValueHandlingBase<float>>();
    }

    private void Start()
    {
        systemFloat.NextValue = startingVextValue;
        SetNextValue(nextValue, time);
    }

    public void SetNextValue(float value, float time)
    {
        systemFloat.SetNextValue(value, time);
    }
}