﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ClosetSubSystemHandler : ValueHandlingFloat
{
    [SerializeField] private GameObject switchSubSystem;
    private IShutOffable shutOffableSubSystem;

    [SerializeField] private GameObject valueSubSystem;
    public IValueChangeableFloat valueChangeableSubSystem;

    private void Awake()
    {
        if (switchSubSystem != null)
        {
            shutOffableSubSystem = switchSubSystem.GetComponent<IShutOffable>();
        }

        if (valueSubSystem != null)
        {
            valueChangeableSubSystem = valueSubSystem.GetComponent<IValueChangeableFloat>();
        }

        RegisterToSubSystemEvents();
    }

    private void RegisterToSubSystemEvents()
    {
        if (valueChangeableSubSystem != null)
        {
            if (valueChangeableSubSystem.OnValueChanged == null)
            {
                valueChangeableSubSystem.OnValueChanged = new FloatEvent();
            }

            valueChangeableSubSystem.OnValueChanged.AddListener(OnSubSystemValueChanged);
        }

        if (shutOffableSubSystem != null)
        {
            if (shutOffableSubSystem.BoolSystemHandler.OnToggleChanged == null)
            {
                shutOffableSubSystem.BoolSystemHandler.OnToggleChanged = new BoolEvent();
            }

            shutOffableSubSystem.BoolSystemHandler.OnToggleChanged.AddListener(OnSubSystemSwitchChanged);
        }
    }

    private void OnSubSystemSwitchChanged(bool isValue)
    {
        if (isValue == false)
        {
            SetNextValue(0, 0);
        }
        else
        {
            if (valueChangeableSubSystem == null)
            {
                SetNextValue(1, 0); //1 is default value
            }
            else
            {
                SetNextValue(valueChangeableSubSystem.Value, 0);
            }
        }
    }

    private void OnSubSystemValueChanged(float value)   //TODO: useless input value
    {
        bool isOn = true;
        if (shutOffableSubSystem != null)
        {
            isOn = shutOffableSubSystem.BoolSystemHandler.IsOn;
        }
        float newValue = 1;
        if (valueChangeableSubSystem != null)
        {
            newValue = valueChangeableSubSystem.Value;
        }

        Value = newValue * (isOn ? 1 : 0);
    }
}