﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoolSystem))]
public class ClosetStateToUIBool : MonoBehaviour, IShutOffable
{
    [SerializeField] private Text closetStateText;
    [SerializeField] private string initialPrefexText;
    private BoolSystem boolSystemHandler;
    public BoolSystem BoolSystemHandler
    {
        get
        {
            if (boolSystemHandler == null)
            {
                boolSystemHandler = GetComponent<BoolSystem>();
            }

            return boolSystemHandler;
        }
        set
        {
            boolSystemHandler = value;
        }
    }
    
    private void Awake()
    {
        initialPrefexText = closetStateText.text;
    }

    private void Start()
    {
        boolSystemHandler.OnToggleChanged.AddListener(UpdateClosetStateText);
        UpdateClosetStateText(boolSystemHandler.SwitchHandling.Value);
    }

    private void UpdateClosetStateText(bool isBoolSystemOn)
    {
        if (isBoolSystemOn)
        {
            closetStateText.text = initialPrefexText + " On";
        }
        else
        {
            closetStateText.text = initialPrefexText + " Off";
        }        
    }
}