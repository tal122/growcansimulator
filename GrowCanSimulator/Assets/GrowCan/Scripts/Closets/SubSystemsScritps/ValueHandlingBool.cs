﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueHandlingBool : ValueHandlingBase<bool>
{
    public BoolEvent OnValueChanged{ get; set; }

    protected override void ValueChanged(bool value)
    {
        if (OnValueChanged != null)
        {
            OnValueChanged.Invoke(value);
        }
    }

    private bool isValue;
    public bool IsValue
    {
        get { return Value; }
        set { Value = value; }
    }

    public void Toggle()
    {
        SetBoolValue(!Value);
    }

    public void SetBoolValue(bool isValue)
    {
        Value = isValue;
    }
    
    public void ActivateEffectedGameObjects(GameObject[] effectedGameObjects)
    {
        for (int i = 0; i < effectedGameObjects.Length; i++)
        {
            effectedGameObjects[i].SetActive(Value);
        }
    }
}