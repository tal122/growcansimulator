﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MockServer : ServerHandler
{
    public override void ConnectToServer()
    {
        Debug.Log("mock connection established");
    }

    public override ClosetState[] GetInitialState()
    {
        ClosetState[] closetsStates = new ClosetState[1];
        closetsStates[0] = GetFakeClosetState(1);
        return closetsStates;
    }
}