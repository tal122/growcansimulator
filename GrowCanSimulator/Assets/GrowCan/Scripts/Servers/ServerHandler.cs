﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandler : MonoBehaviour
{
    protected ClosetState[] closetsState;
    [HideInInspector] public ClosetStateEvent closetStateEvents;
    public virtual void ConnectToServer() { }

    public virtual ClosetState[] GetInitialState()
    {       
        return closetsState;
    }

    protected ClosetState GetFakeClosetState(int closetNumber)
    {
        ClosetState closetState = new ClosetState();
        closetState.timeStamp = new TimeFormat(8, 42, 14);
        //closetState.CoolTube = 1;
        //closetState.Temperature = 4;
        //closetState.IsLightOn = false;
        return closetState;
    }
      
    public void UpdateFakeClosetsStates(ClosetState ClosetsStates)
    {
        Debug.Log("Updated fake closets states on server");
    }
}