﻿using UnityEngine.Events;

internal interface IOnValueChanged
{
    void OnValueChangedRegister(UnityAction<float> function);
}