﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFilled : MonoBehaviour
{
    [HideInInspector] public BoolEvent OnImageFilledDraining;
    [SerializeField] private Image image;
    [SerializeField] private float imageFillAmountSize;
    [SerializeField] private float imageDrainAmountSize;
    private int direction;
    private bool isDrainingImageCurrent, isDrainingImagePrevious;

    private void Awake()
    {
        //this.direction = (int)eFillDirection.DontFill;        
    }

    public void DrainingImage(bool isDrain)//(eFillDirection direction)
    {
        isDrainingImageCurrent = isDrain;
        //this.direction = (int)direction;
        //if (direction == eFillDirection.)
        //{
        //}
    }

    private void LateUpdate()
    {
        image.fillAmount += imageFillAmountSize * Time.deltaTime;
        if (isDrainingImageCurrent)
        {
            image.fillAmount -= imageDrainAmountSize * Time.deltaTime;
            if (OnImageFilledDraining != null)
            {
                if (image.fillAmount == 0)
                {
                    OnImageFilledDraining.Invoke(false);
                }
                else
                {
                    OnImageFilledDraining.Invoke(true);
                }
            }
        }
        else if (isDrainingImagePrevious == true)
        {
            if (OnImageFilledDraining != null)
            {
                OnImageFilledDraining.Invoke(false);
            }
        }

        isDrainingImagePrevious = isDrainingImageCurrent;

        //if (image.fillAmount <= 0)
        //{
        //    image.fillAmount = 0;
        //    OnImageFilled.Invoke(true);
        //}

        //if (direction != 0)
        //{            
        //    OnImageFilled.Invoke(false);
        //}
    }
}

//public enum eFillDirection
//{
//    FillUp = 1,
//    DontFill = 0,
//    FillDown = -1
//}