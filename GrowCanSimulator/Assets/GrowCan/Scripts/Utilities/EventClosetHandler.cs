﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventClosetHandler : MonoBehaviour
{
    private ClosetStateEvent eventStateHandler;

    public void AddListener(UnityAction<ClosetState> unityAction)
    {
        if (eventStateHandler == null)
        {
            eventStateHandler = new ClosetStateEvent();
        }

        eventStateHandler.AddListener(unityAction);
    }

    public void RemoveListener(UnityAction<ClosetState> unityAction)
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.RemoveListener(unityAction);
        }
    }

    public void RemoveAllListener()
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.RemoveAllListeners();
        }
    }

    public void Invoke(ClosetState value)
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.Invoke(value);
        }
    }
}