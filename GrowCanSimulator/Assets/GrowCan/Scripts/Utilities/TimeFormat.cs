﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TimeFormat
{
    public int Seconds, Minutes, Hours;

    public TimeFormat(int seconds, int minutes, int hours)
    {
        this.Seconds = seconds;
        this.Minutes = minutes;
        this.Hours = hours;
    }

    public override string ToString()
    {
        return Hours + ":" + Minutes + ":" + Seconds;
    }
}