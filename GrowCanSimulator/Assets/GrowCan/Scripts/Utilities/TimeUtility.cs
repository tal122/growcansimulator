﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeUtility : MonoBehaviour
{
    private float totalGameSeconds;
    [SerializeField] private float secondsPerSecond;
    [SerializeField] private string initialUIText = "";
    [SerializeField] private Text UITextTime;
    [SerializeField] private GameObject UIGamePaused;
    [SerializeField] private UIFilled UIFilledImage;
    private TimeFormat currentTime;
    public TimeFormat  CurrentTime
    {
        get { return currentTime; }
        set
        {
            currentTime = value;
            if (UITextTime)
            {
                UITextTime.text = initialUIText + currentTime.ToString();
            }
        }
    }

    private void Awake()
    {
        if (UIFilledImage != null && UIFilledImage.OnImageFilledDraining != null)
        {
            UIFilledImage.OnImageFilledDraining.AddListener(OnImageFilledDraining);
        }
    }

    void Start()
    {
        if (UITextTime != null)
        {
            initialUIText = UITextTime.text;
        }

        secondsPerSecond = 1;
        totalGameSeconds += secondsPerSecond * Time.deltaTime;
        UIGamePaused.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        RemoveAsListener();
    }

    private void OnDestroy()
    {
        RemoveAsListener();
    }

    private void RemoveAsListener()
    {
        if (UIFilledImage != null && UIFilledImage.OnImageFilledDraining != null)
        {
            UIFilledImage.OnImageFilledDraining.RemoveListener(OnImageFilledDraining);
        }
    }

    void Update()
    {
        totalGameSeconds += secondsPerSecond * Time.deltaTime;
        CurrentTime = new TimeFormat((int)(totalGameSeconds % 60), (int)((totalGameSeconds / 60) % 60), (int)((totalGameSeconds / 3600) % 24));
    }

    public void SetTimeDate(TimeFormat time)
    {
        totalGameSeconds = time.Seconds + (time.Minutes + time.Hours * 60) * 60;
    }

    public TimeFormat GetTime()
    {
        return CurrentTime;
    }

    public float GetTimeDifferenceInSeconds(TimeFormat nextTime)
    {
        return (CurrentTime.Seconds + (CurrentTime.Minutes + CurrentTime.Hours * 60) * 60) - (nextTime.Seconds + (nextTime.Minutes + nextTime.Hours * 60) * 60);
    }

    public string GetTimeToString(char numericDivider)
    {
        return CurrentTime.Hours.ToString() + numericDivider
            + CurrentTime.Minutes.ToString() + numericDivider
            + ((int)(CurrentTime.Seconds)).ToString();
    }

    //When Image is empty return to normal speed
    private void OnImageFilledDraining(bool isTimeImageFilled)
    {
        if (isTimeImageFilled == true)
        {
            FastForwardTime(10);
        }
        else
        {
            FastForwardTime(1);
        }
    }

    public void FastForwardTime(float timeScale)    //shouldn't be a "stop game" option
    {
        Time.timeScale = timeScale;
        if (timeScale == 0)
        {
            UIGamePaused.gameObject.SetActive(true);
        }
        else
        {
            UIGamePaused.gameObject.SetActive(false);
        }
    }
}