﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnObjectClickPick : MonoBehaviour
{
    [SerializeField] private GameObject UIText;
    private bool isPicked;

    private void Awake()
    {
        UIText.gameObject.SetActive(false);
        isPicked = true;
    }

    private void OnMouseUp()
    {
        isPicked = !isPicked;
        UIText.gameObject.SetActive(!isPicked);
    }
}