﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private bool isMoveable;
    [SerializeField] private float cameraSpeedXYAxis, cameraSpeedZAxis;
    [SerializeField] private float mouseRotationSensitivity = 100.0f;
    [SerializeField] private float clampAngle = 80.0f;
    private float rotationY = 0.0f; // rotation around the up/y axis
    private float rotationX = 0.0f; // rotation around the right/x axis
    private Vector3 cameraRotation;
    private float timeScale;

    void Start()
    {
        isMoveable = true;
        rotationY = cameraRotation.y;
        rotationX = cameraRotation.x;
    }

    private void OnEnable()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        cameraRotation = camera.transform.localRotation.eulerAngles;
    }

    private void LateUpdate()
    {
        if (isMoveable)
        {
            if (camera == null)
            {
                camera = Camera.main;
            }

            if (camera != null || camera.gameObject.activeInHierarchy == true)
            {
                timeScale = (Time.timeScale == 0 ? 1 : Time.timeScale);
                cameraRotation = camera.transform.localRotation.eulerAngles;
                if (!EventSystem.current.IsPointerOverGameObject())
                {

                    if (Input.GetMouseButton(2))
                    {
                        if (Input.GetAxis("Mouse X") != 0)
                        {
                            camera.transform.Translate(Vector3.right * Input.GetAxis("Mouse X") * Time.deltaTime * (cameraSpeedXYAxis / timeScale));
                            camera.transform.Translate(Vector3.up * Input.GetAxis("Mouse Y") * Time.deltaTime * (cameraSpeedXYAxis / timeScale));
                        }
                    }

                    if (Input.GetMouseButton(1))
                    {
                        if (Input.GetAxis("Mouse X") != 0)
                        {
                            float mousePositionX = Input.GetAxis("Mouse X");
                            float mousePositionY = -Input.GetAxis("Mouse Y");
                            rotationY += mousePositionX * (mouseRotationSensitivity / timeScale) * Time.deltaTime;
                            rotationX += mousePositionY * (mouseRotationSensitivity / timeScale) * Time.deltaTime;
                            rotationX = Mathf.Clamp(rotationX, -clampAngle, clampAngle);    //TODO: doesnt seem to work
                            Quaternion localRotation = Quaternion.Euler(rotationX, rotationY, 0.0f);
                            camera.transform.rotation = Quaternion.Slerp(camera.transform.rotation, localRotation, Time.deltaTime * (mouseRotationSensitivity / timeScale));
                        }
                    }

                    if (Input.GetAxis("Mouse ScrollWheel") != 0)
                    {
                        camera.transform.Translate(Vector3.forward * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * (cameraSpeedZAxis / timeScale));
                    }
                }
            }
        }
    }

    public void EnableMovement(bool isEnable)
    {
        isMoveable = isEnable;
    }
}