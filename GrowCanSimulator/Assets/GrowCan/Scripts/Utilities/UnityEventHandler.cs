﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventHandler
{
    private UnityEvent eventStateHandler;

    public void AddListener(UnityAction unityAction)
    {
        if (eventStateHandler == null)
        {
            eventStateHandler = new UnityEvent();
        }

        eventStateHandler.AddListener(unityAction);
    }

    public void RemoveListener(UnityAction unityAction)
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.RemoveListener(unityAction);
        }
    }

    public void RemoveAllListener()
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.RemoveAllListeners();
        }
    }

    public void Invoke()
    {
        if (eventStateHandler != null)
        {
            eventStateHandler.Invoke();
        }
    }
}