﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class OnObjectClickLevels : MonoBehaviour, IOnObjectClickBase<float>, IOnObjectClickBase<bool>
{
    private UnityEvent<bool> OnObjectClickedBool { get; set; }
    private UnityEvent<float> OnObjectClickedFloat { get; set; }
    [SerializeField] private int numberOfLevels;
    [SerializeField] private int value;
    public int Value
    {
        get { return value; }
        set { this.value = value; }
    }

    public void AddListener(UnityAction<float> unityActions)
    {
        if (OnObjectClickedFloat == null)
        {
            OnObjectClickedFloat = new FloatEvent();
        }

        OnObjectClickedFloat.AddListener(unityActions);
    }

    public void AddListener(UnityAction<bool> unityActions)
    {
        if (OnObjectClickedBool == null)
        {
            OnObjectClickedBool = new BoolEvent();
        }

        OnObjectClickedBool.AddListener(unityActions);
    }

    private void ObjectClicked()
    {
        Value++;
        if (Value == numberOfLevels)
        {
            Value = 0;
            if (OnObjectClickedBool != null)
            {
                OnObjectClickedBool.Invoke(false);
            }
        }
        else
        {
            if (OnObjectClickedBool != null)
            {
                OnObjectClickedBool.Invoke(true);
            }
        }

        if (OnObjectClickedFloat != null)
        {
            OnObjectClickedFloat.Invoke(Value);
        }
    }

    private void OnMouseDown()
    {
        ObjectClicked();
    }
}