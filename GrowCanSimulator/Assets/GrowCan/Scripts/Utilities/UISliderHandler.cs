﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISliderHandler : ValueHandlingFloat
{
    [SerializeField] private Slider slider;

    private void Awake()
    {
        slider.onValueChanged.AddListener(ChangeNextValue);
    }

    protected void ChangeNextValue(float newValue)
    {
        SetNextValue(newValue, 1.5f);
        //NextValue = newValue;
    }
}