﻿using System;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ClosetStateEvent : UnityEvent<ClosetState>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}

[System.Serializable]
public class BoolEvent : UnityEvent<bool>
{
}

[System.Serializable]
public class Vector2Event : UnityEvent<float, float>
{
}

[System.Serializable]
public class Vector3Event : UnityEvent<float, float, float>
{
}