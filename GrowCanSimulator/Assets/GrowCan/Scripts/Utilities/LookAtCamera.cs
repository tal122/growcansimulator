﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField] private Camera camera;

    private void Update()
    {
        transform.LookAt(camera.transform.position, Vector3.up);
    }
}