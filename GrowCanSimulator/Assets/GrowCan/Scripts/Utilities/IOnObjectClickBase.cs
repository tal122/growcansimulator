﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IOnObjectClickBase<T>
{
    void AddListener(UnityAction<T> listener);
}
