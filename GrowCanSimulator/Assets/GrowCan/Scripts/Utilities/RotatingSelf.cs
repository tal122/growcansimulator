﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingSelf : MonoBehaviour
{
    [SerializeField] private GameObject rotatingObject;
    [SerializeField] private float fanSpeed;

    void Update()
    {
        rotatingObject.transform.Rotate(Vector3.up * fanSpeed * Time.deltaTime, Space.Self);
    }
}
