﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class OnObjectClick : MonoBehaviour, IOnObjectClickBase<bool>
{
    private UnityEvent<bool> OnObjectClicked { get; set; }
    private bool value;
    private bool Value
    {
        get { return value; }
        set { this.value = value; }
    }

    public void AddListener(UnityAction<bool> listener)
    {
        if (OnObjectClicked == null)
        {
            OnObjectClicked = new BoolEvent();
        }

        OnObjectClicked.AddListener(listener);
    }

    private void ObjectClicked()
    {
        Value = !Value;
        if (OnObjectClicked != null)
        {
            OnObjectClicked.Invoke(Value);
        }
    }

    private void OnMouseDown()
    {
        ObjectClicked();
    }
}