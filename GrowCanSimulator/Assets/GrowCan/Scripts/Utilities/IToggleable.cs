﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IToggleable
{
    void Toggle();
    void SetValue(bool isValue);
    BoolEvent OnToggleChanged { get; set; }
}
