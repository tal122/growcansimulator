﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum eCannabisType
{
    sativa,
    indica,
    ruderalis
}

public class CannabisTypesHandler : MonoBehaviour
{
    private eCannabisType currentCannabisType;

    public void SetCannaisType(int cannabisType)
    {
        currentCannabisType = (eCannabisType)cannabisType;
    }        
}